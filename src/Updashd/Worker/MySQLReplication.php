<?php
namespace Updashd\Worker;

use Updashd\Configlib\Config;
use Updashd\Worker\Exception\WorkerConfigurationException;

class MySQLReplication extends AbstractMySQLWorker {
    const CONFIG_FIELD_INCLUDE_MASTER_DETAILS = 'include_master_details';
    const CONFIG_FIELD_ALWAYS_INCLUDE_ERRORS = 'always_include_errors';

    const METRIC_MASTER_HOST = 'master_host';
    const METRIC_MASTER_PORT = 'master_port';
    const METRIC_MASTER_USER = 'master_user';
    const METRIC_MASTER_LOG_FILE = 'master_log_file';
    const METRIC_MASTER_LOG_POS = 'master_log_pos';
    const METRIC_SLAVE_IO_RUNNING = 'slave_io_running';
    const METRIC_SLAVE_IO_STATE = 'slave_io_state';
    const METRIC_SLAVE_SQL_RUNNING = 'slave_sql_running';
    const METRIC_EXEC_MASTER_LOG_POS = 'exec_master_log_pos';
    const METRIC_SECONDS_BEHIND_MASTER = 'seconds_behind_master';
    const METRIC_LAST_IO_ERRNO = 'last_io_errno';
    const METRIC_LAST_IO_ERROR = 'last_io_error';
    const METRIC_LAST_SQL_ERRNO = 'last_sql_errno';
    const METRIC_LAST_SQL_ERROR = 'last_sql_error';

    const GROUP_REPLICATION_CHECK_OPTIONS = 'replication_check_options';

    protected $config;

    /**
     * Get the readable name of the service
     */
    public static function getReadableName () {
        return 'MySQL Replication';
    }

    /**
     * Get the name of the service (this should match in the database)
     * @return string
     * @throws \Updashd\Worker\Exception\WorkerConfigurationException
     */
    public static function getServiceName () {
        return 'mysql_replication';
    }

    public static function createResult () {
        $result = parent::createResult();

        // Master Details
        $result->addMetricString(self::METRIC_MASTER_HOST, 'Master Host');
        $result->addMetricInt(self::METRIC_MASTER_PORT, 'Master Port');
        $result->addMetricString(self::METRIC_MASTER_USER, 'Master User');
        $result->addMetricString(self::METRIC_MASTER_LOG_FILE, 'Master Log File');
        $result->addMetricInt(self::METRIC_MASTER_LOG_POS, 'Master Log Pos', 'bytes');
        $result->addMetricInt(self::METRIC_EXEC_MASTER_LOG_POS, 'Master Log Pos', 'bytes');

        // Status Details
        $result->addMetricInt(self::METRIC_SLAVE_IO_RUNNING, 'Slave IO Running', 'running');   // Cast to bool
        $result->addMetricInt(self::METRIC_SLAVE_SQL_RUNNING, 'Slave SQL Running', 'running');  // Cast to bool
        $result->addMetricInt(self::METRIC_SECONDS_BEHIND_MASTER, 'Seconds Behind Master');

        // IO/SQL Error Info
        $result->addMetricString(self::METRIC_SLAVE_IO_STATE, 'Slave IO State');
        $result->addMetricInt(self::METRIC_LAST_IO_ERRNO, 'Last IO Errno');
        $result->addMetricString(self::METRIC_LAST_IO_ERROR, 'Last IO Error');
        $result->addMetricInt(self::METRIC_LAST_SQL_ERRNO, 'Last SQL Errno');
        $result->addMetricString(self::METRIC_LAST_SQL_ERROR, 'Last SQL Error');

        return $result;
    }

    public static function createConfig () {
        $config = parent::createConfig();

        $config->addFieldCheckbox(self::CONFIG_FIELD_INCLUDE_MASTER_DETAILS, 'Include Master Details', false);
        $config->addFieldCheckbox(self::CONFIG_FIELD_ALWAYS_INCLUDE_ERRORS, 'Always Include Errors', false);

        $config->addGroup(self::GROUP_REPLICATION_CHECK_OPTIONS, 'Replication Check Options');
        $config->addFieldToGroup(self::GROUP_REPLICATION_CHECK_OPTIONS, self::CONFIG_FIELD_INCLUDE_MASTER_DETAILS);
        $config->addFieldToGroup(self::GROUP_REPLICATION_CHECK_OPTIONS, self::CONFIG_FIELD_ALWAYS_INCLUDE_ERRORS);

        return $config;
    }

    /**
     * Run the given test
     * @return Result
     * @throws WorkerConfigurationException
     */
    public function run () {
        $config = $this->getConfig();

        $result = self::createResult();

        try {
            $pdo = $this->getPDO($config, $result);

            $statement = $pdo->query($this->getQuery());

            $row = $statement->fetch(\PDO::FETCH_ASSOC);

            if (! $row) {
                throw new WorkerConfigurationException('No replication is configured on slave.', 100);
            }

            if ($config->getValue(self::CONFIG_FIELD_INCLUDE_MASTER_DETAILS, true)) {
                $result->setMetricValue(self::METRIC_MASTER_HOST, $row['Master_Host']);
                $result->setMetricValue(self::METRIC_MASTER_PORT, $row['Master_Port']);
                $result->setMetricValue(self::METRIC_MASTER_USER, $row['Master_User']);
                $result->setMetricValue(self::METRIC_MASTER_LOG_FILE, $row['Master_Log_file']);
                $result->setMetricValue(self::METRIC_MASTER_LOG_POS, $row['Read_Master_Log_pos']);
                $result->setMetricValue(self::METRIC_EXEC_MASTER_LOG_POS, $row['Exec_Master_Log_Pos']);
            }

            $ioRunning = $row['Slave_IO_Running'] == 'Yes';
            $sqlRunning = $row['Slave_SQL_Running'] == 'Yes';
            $secondsBehind = $row['Seconds_Behind_Master'];

            $result->setMetricValue(self::METRIC_SLAVE_IO_RUNNING, (int) $ioRunning);
            $result->setMetricValue(self::METRIC_SLAVE_SQL_RUNNING, (int) $sqlRunning);
            $result->setMetricValue(self::METRIC_SECONDS_BEHIND_MASTER, (int) $secondsBehind);

            if ($config->getValue(self::CONFIG_FIELD_ALWAYS_INCLUDE_ERRORS, true)) {
                $result->setMetricValue(self::METRIC_SLAVE_IO_STATE, $row['Slave_IO_State']);
                $result->setMetricValue(self::METRIC_LAST_IO_ERRNO, $row['Last_IO_Errno']);
                $result->setMetricValue(self::METRIC_LAST_IO_ERROR, $row['Last_IO_Error']);
                $result->setMetricValue(self::METRIC_LAST_SQL_ERRNO, $row['Last_SQL_Errno']);
                $result->setMetricValue(self::METRIC_LAST_SQL_ERROR, $row['Last_SQL_Error']);
            }

            if ($ioRunning && $sqlRunning) {
                $result->setStatus(Result::STATUS_SUCCESS);
            }
            else {
                $result->setStatus(Result::STATUS_CRITICAL);
                $code = ($ioRunning << 1)|($sqlRunning);
                $reason = ''
                    . (! $ioRunning ? ' Slave_IO_Running is "' . $row['Slave_IO_Running'] . '"' : '')
                    . (! $sqlRunning ? ' Slave_SQL_Running is "' . $row['Slave_SQL_Running'] . '"' : '');
                $result->setErrorMessage('Replication status is bad: ' . $reason);
                $result->setErrorCode($code);
            }
        }
        catch (\PDOException $e) {
            $result->setStatus(Result::STATUS_CRITICAL);
            $result->setErrorCode($e->getCode());
            $result->setErrorMessage($e->getMessage());
        }

        return $result;
    }

    public function getQuery () {
        return "SHOW SLAVE STATUS";
    }
}