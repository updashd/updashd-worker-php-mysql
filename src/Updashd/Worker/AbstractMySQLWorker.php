<?php
namespace Updashd\Worker;

use Updashd\Configlib\Config;
use Updashd\Configlib\Validator\HostnameValidator;
use Updashd\Configlib\Validator\Ip4Validator;
use Updashd\Configlib\Validator\PortNumberValidator;
use Updashd\Worker\Pdo\MySQL;

abstract class AbstractMySQLWorker implements WorkerInterface {
    const CONFIG_FIELD_HOSTNAME = 'hostname';
    const CONFIG_FIELD_IP = 'ip';
    const CONFIG_FIELD_PREFER_IP = 'prefer_ip';
    const CONFIG_FIELD_PORT = 'port';
    const CONFIG_FIELD_USERNAME = 'username';
    const CONFIG_FIELD_PASSWORD = 'password';
    const CONFIG_FIELD_DATABASE = 'database';
    const CONFIG_FIELD_USE_SSL = 'use_ssl';
    const CONFIG_FIELD_SSL_CA_CERT = 'ssl_ca_cert';
    const CONFIG_FIELD_SSL_CLIENT_CERT = 'ssl_client_cert';
    const CONFIG_FIELD_SSL_CLIENT_KEY = 'ssl_client_key';
    const CONFIG_FIELD_SSL_CIPHER = 'ssl_cipher';

    const METRIC_CONNECT_TIME = 'connect_time';

    const GROUP_NETWORK = 'network';
    const GROUP_DATABASE = 'database';
    const GROUP_SSL = 'ssl';

    protected $config;

    /**
     * Create a worker instance for the given service
     * @param Config $config the Config object used for configuration
     * @throws \Updashd\Worker\Exception\WorkerConfigurationException
     */
    public function __construct (Config $config) {
        $this->setConfig($config);
    }

    public static function createConfig () {
        $config = new Config();

        $config->addFieldText(self::CONFIG_FIELD_HOSTNAME, 'Host Name', null, true)
            ->setReferenceField('hostname')
            ->addValidator(new HostnameValidator());

        $config->addFieldText(self::CONFIG_FIELD_IP, 'IP')
            ->setReferenceField('ip')
            ->addValidator(new Ip4Validator());;

        $config->addFieldCheckbox(self::CONFIG_FIELD_PREFER_IP, 'Prefer IP', false, true);

        $config->addFieldNumber(self::CONFIG_FIELD_PORT, 'Port', 3306, true)
            ->addValidator(new PortNumberValidator());

        $config->addFieldText(self::CONFIG_FIELD_USERNAME, 'User Name');

        $config->addFieldPassword(self::CONFIG_FIELD_PASSWORD, 'Password');

        $config->addFieldText(self::CONFIG_FIELD_DATABASE, 'Database');


        // Groups
        $config->addGroup(self::GROUP_NETWORK, 'Network');
        $config->addFieldToGroup(self::GROUP_NETWORK, self::CONFIG_FIELD_HOSTNAME);
        $config->addFieldToGroup(self::GROUP_NETWORK, self::CONFIG_FIELD_IP);
        $config->addFieldToGroup(self::GROUP_NETWORK, self::CONFIG_FIELD_PREFER_IP);
        $config->addFieldToGroup(self::GROUP_NETWORK, self::CONFIG_FIELD_PORT);

        $config->addGroup(self::GROUP_DATABASE, 'Database');
        $config->addFieldToGroup(self::GROUP_DATABASE, self::CONFIG_FIELD_USERNAME);
        $config->addFieldToGroup(self::GROUP_DATABASE, self::CONFIG_FIELD_PASSWORD);
        $config->addFieldToGroup(self::GROUP_DATABASE, self::CONFIG_FIELD_DATABASE);

        $config->addFieldCheckbox(self::CONFIG_FIELD_USE_SSL, 'Use SSL', false);
        $config->addFieldMultiLineText(self::CONFIG_FIELD_SSL_CA_CERT, 'CA Certificate');
        $config->addFieldMultiLineText(self::CONFIG_FIELD_SSL_CLIENT_CERT, 'Client Certificate');
        $config->addFieldMultiLineText(self::CONFIG_FIELD_SSL_CLIENT_KEY, 'Client Key');
        $config->addFieldText(self::CONFIG_FIELD_SSL_CIPHER, 'Cipher (OpenSSL)');

        $config->addGroup(self::GROUP_SSL, 'SSL Settings');
        $config->addFieldToGroup(self::GROUP_SSL, self::CONFIG_FIELD_USE_SSL);
        $config->addFieldToGroup(self::GROUP_SSL, self::CONFIG_FIELD_SSL_CA_CERT);
        $config->addFieldToGroup(self::GROUP_SSL, self::CONFIG_FIELD_SSL_CLIENT_CERT);
        $config->addFieldToGroup(self::GROUP_SSL, self::CONFIG_FIELD_SSL_CLIENT_KEY);
        $config->addFieldToGroup(self::GROUP_SSL, self::CONFIG_FIELD_SSL_CIPHER);

        return $config;
    }

    public static function createResult () {
        $result = new Result();

        $result->addMetricFloat(self::METRIC_CONNECT_TIME, 'Connect Time', 'sec');

        return $result;
    }

    /**
     * @param Config $config
     * @param Result $result
     * @return null|MySQL
     */
    public function getPDO (Config $config, Result $result) {

        if ($config->getValue(self::CONFIG_FIELD_PREFER_IP)) {
            $host = $config->getValue(self::CONFIG_FIELD_PREFER_IP, true);
        }
        else {
            $host = $config->getValue(self::CONFIG_FIELD_HOSTNAME, true);
        }

        $port = $config->getValue(self::CONFIG_FIELD_PORT, true);
        $username = $config->getValue(self::CONFIG_FIELD_USERNAME, true);
        $password = $config->getValue(self::CONFIG_FIELD_PASSWORD, true);
        $database = $config->getValue(self::CONFIG_FIELD_DATABASE, true);
        $options = [];

        if ($config->getValue(self::CONFIG_FIELD_USE_SSL)) {
            // Create temporary files for the ca, cert, and key
            $options[\PDO::MYSQL_ATTR_SSL_KEY] = $this->createTmpFileForConfig($config, self::CONFIG_FIELD_SSL_CLIENT_KEY);
            $options[\PDO::MYSQL_ATTR_SSL_CERT] = $this->createTmpFileForConfig($config, self::CONFIG_FIELD_SSL_CLIENT_CERT);
            $options[\PDO::MYSQL_ATTR_SSL_CA] = $this->createTmpFileForConfig($config, self::CONFIG_FIELD_SSL_CLIENT_CERT);

            // Custom Cipher
            $cipher = $config->getValue(self::CONFIG_FIELD_SSL_CIPHER);

            if ($cipher) {
                $options[\PDO::MYSQL_ATTR_SSL_CIPHER] = $cipher;
            }
        }

        $timeStart = microtime(true);

        try {
            $pdo = new MySQL($host, $port, $username, $password, $database, $options);

            return $pdo;
        }
        catch (\PDOException $e) {
            throw $e;
        }
        finally {
            // Remove temporary files for ca, cert, and key (if created)
            $this->removeTmpFileByArrayKey($options, \PDO::MYSQL_ATTR_SSL_KEY);
            $this->removeTmpFileByArrayKey($options, \PDO::MYSQL_ATTR_SSL_CERT);
            $this->removeTmpFileByArrayKey($options, \PDO::MYSQL_ATTR_SSL_CA);

            $timeEnd = microtime(true);

            $time = $timeEnd - $timeStart;

            $result->setMetricValue(self::METRIC_CONNECT_TIME, $time);
        }
    }

    /**
     * @param Config $config
     * @param string $key
     * @return bool|string
     */
    protected function createTmpFileForConfig ($config, $key) {
        $sysTmpDir = sys_get_temp_dir();

        $filename = tempnam($sysTmpDir, $this->generateRandomString());

        file_put_contents($filename, $config->getValue($key));

        return $filename;
    }

    protected function removeTmpFileByArrayKey ($options, $key) {
        if (array_key_exists($key, $options) && $options[$key]) {
            unlink($options[$key]);
        }
    }

    protected function generateRandomString ($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * @return Config
     */
    protected function getConfig () {
        return $this->config;
    }

    /**
     * @param Config $config
     */
    protected function setConfig (Config $config) {
        $this->config = $config;
    }
}