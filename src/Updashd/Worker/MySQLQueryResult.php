<?php
namespace Updashd\Worker;

use Updashd\Worker\Exception\WorkerConfigurationException;

class MySQLQueryResult extends AbstractMySQLWorker {
    const CONFIG_FIELD_QUERY = 'query';
    const CONFIG_FIELD_INCLUDE_RESULTS_AS_JSON = 'include_results_as_json';

    const GROUP_QUERY_OPTIONS = 'query_options';

    const METRIC_TXT_RESULT = 'txt_result';
    const METRIC_FLOAT_RESULT = 'float_result';
    const METRIC_INT_RESULT = 'int_result';
    const METRIC_STR_RESULT = 'str_result';
    const METRIC_JSON_RESULT = 'json_result';

    const MAX_JSON_SIZE = 16 * 1024; // 16k

    const QUERY_FIELD_STATUS = 'status';
    const QUERY_FIELD_ERROR_MESSAGE = 'error_message';
    const QUERY_FIELD_ERROR_CODE = 'error_code';

    protected $config;

    /**
     * Get the readable name of the service
     */
    public static function getReadableName () {
        return 'MySQL Query Result';
    }

    /**
     * Get the name of the service (this should match in the database)
     * @return string
     * @throws \Updashd\Worker\Exception\WorkerConfigurationException
     */
    public static function getServiceName () {
        return 'mysql_query_result';
    }

    public static function createResult () {
        $result = parent::createResult();

        $result->addMetricText(self::METRIC_JSON_RESULT, 'JSON Result');

        $result->addMetricText(self::METRIC_TXT_RESULT, 'Text Result');
        $result->addMetricFloat(self::METRIC_FLOAT_RESULT, 'Float Result');
        $result->addMetricInt(self::METRIC_INT_RESULT, 'Integer Result');
        $result->addMetricString(self::METRIC_STR_RESULT, 'String Result');

        return $result;
    }

    public static function createConfig () {
        $config = parent::createConfig();

        $config->addFieldMultiLineText(self::CONFIG_FIELD_QUERY, 'Query', self::getDefaultQuery(), true);

        $config->addFieldCheckbox(self::CONFIG_FIELD_INCLUDE_RESULTS_AS_JSON, 'Include Results as JSON', false);

        $config->addGroup(self::GROUP_QUERY_OPTIONS, 'Query Options');
        $config->addFieldToGroup(self::GROUP_QUERY_OPTIONS, self::CONFIG_FIELD_QUERY);
        $config->addFieldToGroup(self::GROUP_QUERY_OPTIONS, self::CONFIG_FIELD_INCLUDE_RESULTS_AS_JSON);

        return $config;
    }

    /**
     * Run the given test
     * @return Result
     * @throws WorkerConfigurationException
     */
    public function run () {
        $config = $this->getConfig();

        $result = self::createResult();

        try {
            $pdo = $this->getPDO($config, $result);

            $statement = $pdo->query($config->getValueRequired(self::CONFIG_FIELD_QUERY, false));

            $queryResult = $statement->fetch(\PDO::FETCH_ASSOC);

            if ($queryResult) {
                $result->setStatus(Result::STATUS_SUCCESS);

                $jsonString = json_encode($queryResult, JSON_NUMERIC_CHECK);

                if (strlen($jsonString) < self::MAX_JSON_SIZE && $config->getValue(self::CONFIG_FIELD_INCLUDE_RESULTS_AS_JSON, true)) {
                    $result->setMetricValue(self::METRIC_JSON_RESULT, $jsonString);
                }

                if (! isset($queryResult[self::QUERY_FIELD_STATUS])) {
                    throw new WorkerConfigurationException('"status" field is required and was not provided by the query result.');
                }

                $status = $queryResult[self::QUERY_FIELD_STATUS];

                if (! in_array($status, self::getStatusOptions())) {
                    throw new WorkerConfigurationException('"status" value is not found in the list of acceptable result statuses.');
                }

                $result->setStatus($status);

                if ($status != Result::STATUS_SUCCESS) {
                    $result->setErrorMessage($this->getArrayValue($queryResult, self::QUERY_FIELD_ERROR_MESSAGE));
                    $result->setErrorCode($this->getArrayValue($queryResult, self::QUERY_FIELD_ERROR_CODE));
                }

                $result->setMetricValue(self::METRIC_INT_RESULT, $this->getArrayValue($queryResult, self::METRIC_INT_RESULT));
                $result->setMetricValue(self::METRIC_TXT_RESULT, $this->getArrayValue($queryResult, self::METRIC_TXT_RESULT));
                $result->setMetricValue(self::METRIC_STR_RESULT, $this->getArrayValue($queryResult, self::METRIC_STR_RESULT));
                $result->setMetricValue(self::METRIC_FLOAT_RESULT, $this->getArrayValue($queryResult, self::METRIC_FLOAT_RESULT));
            }
            else {
                $result->setStatus(Result::STATUS_CRITICAL);
                $result->setErrorMessage('Query returned no results.');
                $result->setErrorCode(0);
            }
        }
        catch (\PDOException $e) {
            throw new WorkerConfigurationException($e->getMessage(), $e->getCode());
        }

        return $result;
    }

    protected function getArrayValue ($array, $key) {
        return (array_key_exists($key, $array) ? $array[$key] : null);
    }

    /**
     * @return array
     */
    protected static function getStatusOptions () {
        return [
            Result::STATUS_SUCCESS,
            Result::STATUS_NOTICE,
            Result::STATUS_WARNING,
            Result::STATUS_CRITICAL
        ];
    }

    protected static function getDefaultQuery () {
        $sql = /** @lang text */
            <<<SQL
SELECT 
    '{STATUS_SUCCESS}' AS {QUERY_FIELD_STATUS} -- REQUIRED!!! {STATUS_OPTIONS}
    5 AS {METRIC_INT_RESULT},
    4.5 AS {METRIC_FLOAT_RESULT},
    'Hello, World!' AS {METRIC_STR_RESULT},
    'Pretend this is a very long string that needs a TXT field.' AS {METRIC_TXT_RESULT},
    'something went terribly wrong!' AS {QUERY_FIELD_ERROR_MESSAGE},
    15 AS {QUERY_FIELD_ERROR_CODE},
    RAND() * 100 AS random_value_for_json
LIMIT 1 -- Only one row of the result will be considered
SQL;
        $map = [
            'STATUS_SUCCESS' => Result::STATUS_SUCCESS,
            'STATUS_OPTIONS' => implode(', ', self::getStatusOptions()),

            'METRIC_INT_RESULT' => self::METRIC_INT_RESULT,
            'METRIC_FLOAT_RESULT' => self::METRIC_FLOAT_RESULT,
            'METRIC_STR_RESULT' => self::METRIC_STR_RESULT,
            'METRIC_TXT_RESULT' => self::METRIC_TXT_RESULT,

            'QUERY_FIELD_STATUS' => self::QUERY_FIELD_STATUS,
            'QUERY_FIELD_ERROR_CODE' => self::QUERY_FIELD_ERROR_CODE,
            'QUERY_FIELD_ERROR_MESSAGE' => self::QUERY_FIELD_ERROR_MESSAGE,
        ];

        return str_replace(array_map(function ($e) { return '{' . $e . '}'; }, array_keys($map)), array_values($map), $sql);
    }
}