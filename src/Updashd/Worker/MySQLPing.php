<?php
namespace Updashd\Worker;

use Updashd\Configlib\Config;
use Updashd\Worker\Exception\WorkerConfigurationException;

class MySQLPing extends AbstractMySQLWorker {
    const CONFIG_FIELD_METHOD = 'method';

    const GROUP_PING_OPTIONS = 'ping_options';

    /**
     * Get the readable name of the service
     */
    public static function getReadableName () {
        return 'MySQL Ping';
    }

    /**
     * Get the name of the service (this should match in the database)
     * @return string
     * @throws \Updashd\Worker\Exception\WorkerConfigurationException
     */
    public static function getServiceName () {
        return 'mysql_ping';
    }

    public static function createResult () {
        $result = parent::createResult();

        return $result;
    }

    public static function createConfig () {
        $config = parent::createConfig();

        $config->addFieldSelect(self::CONFIG_FIELD_METHOD, 'Ping Method', self::getMethodOptions(), 'select1');

        $config->addGroup(self::GROUP_PING_OPTIONS, 'Ping Options');
        $config->addFieldToGroup(self::GROUP_PING_OPTIONS, self::CONFIG_FIELD_METHOD);

        return $config;
    }

    protected static function getMethodOptions () {
        return [
            'select1' => 'SELECT 1',
            'select_now' => 'SELECT NOW()',
        ];
    }

    /**
     * Run the given test
     * @return Result
     * @throws WorkerConfigurationException
     */
    public function run () {
        $config = $this->getConfig();

        $result = self::createResult();

        try {
            $pdo = $this->getPDO($config, $result);

            $statement = $pdo->query($this->getPingQuery());

            $firstColumn = $statement->fetchColumn(0);

            if ($firstColumn) {
                $result->setStatus(Result::STATUS_SUCCESS);
            }
            else {
                $result->setStatus(Result::STATUS_CRITICAL);
                $result->setErrorMessage('Ping result is bad: ' . $firstColumn);
                $result->setErrorCode(0);
            }
        }
        catch (\PDOException $e) {
            $result->setStatus(Result::STATUS_CRITICAL);
            $result->setErrorCode($e->getCode());
            $result->setErrorMessage($e->getMessage());
        }

        return $result;
    }

    protected function getPingQuery () {
        $options = self::getMethodOptions();

        $key = $this->getConfig()->getValueRequired(self::CONFIG_FIELD_METHOD, true);

        return isset($options[$key]) ? $options[$key] : $options['select1'];
    }
}