<?php
namespace Updashd\Worker\Pdo;

class MySQL extends \PDO {
    protected $hostname;
    protected $port;
    protected $username;
    protected $password;
    protected $database;
    protected $options;

    /**
     * MySQL constructor.
     * @param string $hostname
     * @param int $port
     * @param string $username
     * @param string $password
     * @param string $database
     * @param array $options
     */
    public function __construct ($hostname, $port, $username, $password, $database, $options) {
        $this->setHostname($hostname);
        $this->setPort($port);
        $this->setUsername($username);
        $this->setPassword($password);
        $this->setDatabase($database);
        $this->setOptions($options);

        parent::__construct($this->getDsn(), $this->getUsername(), $this->getPassword(), $this->getOptions());
    }

    /**
     * @return string
     */
    protected function getDsn () {
        $str = 'mysql:';

        $options = [];

        if ($this->getHostname()) {
            $options[] = 'host=' . $this->getHostname();
        }

        if ($this->getPort()) {
            $options[] = 'port=' . $this->getPort();
        }

        if ($this->getDatabase()) {
            $options[] = 'dbname=' . $this->getDatabase();
        }

        $str .= implode(';', $options);

        return $str;
    }

    /**
     * @return string
     */
    public function getHostname () {
        return $this->hostname;
    }

    /**
     * @param string $hostname
     */
    public function setHostname ($hostname) {
        $this->hostname = $hostname;
    }

    /**
     * @return int
     */
    public function getPort () {
        return $this->port;
    }

    /**
     * @param int $port
     */
    public function setPort ($port) {
        $this->port = $port;
    }

    /**
     * @return string
     */
    public function getUsername () {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername ($username) {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getPassword () {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword ($password) {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getDatabase () {
        return $this->database;
    }

    /**
     * @param string $database
     */
    public function setDatabase ($database) {
        $this->database = $database;
    }

    /**
     * @return array
     */
    public function getOptions () {
        return $this->options;
    }

    /**
     * @param array $options
     */
    public function setOptions ($options) {
        // Always force ERRMODE to be ERRMODE_EXCEPTION
        $options[\PDO::ATTR_ERRMODE] = \PDO::ERRMODE_EXCEPTION;

        $this->options = $options;
    }
}