Updashd Worker PHP - MySQL Service Checks
=========================================

This module provides the following checks:
- Ping
- Query Execution Time
- Query Result
- Replication
