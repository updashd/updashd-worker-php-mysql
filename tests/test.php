<?php
require __DIR__ . '/../vendor/autoload.php';

// Replication Check
$config = \Updashd\Worker\MySQLReplication::createConfig();

$config->setValue(\Updashd\Worker\MySQLReplication::CONFIG_FIELD_HOSTNAME, 'localhost');
$config->setValue(\Updashd\Worker\MySQLReplication::CONFIG_FIELD_USERNAME, 'root');
$config->setValue(\Updashd\Worker\MySQLReplication::CONFIG_FIELD_PASSWORD, '');
$config->setValue(\Updashd\Worker\MySQLReplication::CONFIG_FIELD_INCLUDE_MASTER_DETAILS, true);
$config->setValue(\Updashd\Worker\MySQLReplication::CONFIG_FIELD_ALWAYS_INCLUDE_ERRORS, true);

$replicationCheck = new \Updashd\Worker\MySQLReplication($config);

$result = $replicationCheck->run();

print_r($result->toArray());


// Ping Check
$config = \Updashd\Worker\MySQLPing::createConfig();

$config->setValue(\Updashd\Worker\MySQLPing::CONFIG_FIELD_HOSTNAME, 'localhost');
$config->setValue(\Updashd\Worker\MySQLPing::CONFIG_FIELD_USERNAME, 'root');
$config->setValue(\Updashd\Worker\MySQLPing::CONFIG_FIELD_PASSWORD, '');
$config->setValue(\Updashd\Worker\MySQLPing::CONFIG_FIELD_METHOD, 'select1');

$replicationCheck = new \Updashd\Worker\MySQLPing($config);

$result = $replicationCheck->run();

print_r($result->toArray());